# RVM Config
$:.unshift(File.expand_path('./lib', ENV['rvm_path'])) # Add RVM's lib directory to the load path.
set :rvm_ruby_string, '1.9.3-p0@global'

require "rvm/capistrano"
require "bundler/capistrano"

# Deploy Server Config
set :user, "deploy"
ssh_options[:keys] = ["#{ENV['HOME']}/.ssh/deploy"]
set :use_sudo, false

# Server Config
default_run_options[:pty] = true
set :application, 'engineering-calculator'
set :rails_env, "production"
role :web,      "engineering-calculator.com"
role :app,      "engineering-calculator.com"
role :memcache, "engineering-calculator.com"

# Git Config
set :repository,  "https://tmuntan1@bitbucket.org/tmuntan1/engineering-calculator.git"
set :scm,         "git"
set :branch,      "master"
set :deploy_via,  :remote_cache
set :deploy_to,   "/var/www/apps/engineering-calculator"

namespace :passenger do
  desc "Restart Application"  
  task :restart do  
    run "touch #{current_path}/tmp/restart.txt"  
  end
end

after :deploy, "passenger:restart"