class EngineerController < ApplicationController
  
  def index
    @title = "Calculators"
  end
  
  def fanno
    @title = "Fanno Flow"
    meta :title => @title, :description => "Solves Fanno Flow Problems", :keywords => %w(Engineering Calculator Fanno Flow)
  end
  
  def isentropic
    @title = "Isentropic Flow"
    meta :title => @title, :description => "Solves Isentropic Flow Problems", :keywords => %w(Engineering Calculator Isentropic Flow)
  end
  
  def normal_shock
    @title = "Normal Shock"
    meta :title => @title, :description => "Solves Normal Shock Problems", :keywords => %w(Engineering Calculator Shock)
  end
  
  def oblique
    @title = "Oblique Shock"
    meta :title => @title, :description => "Solves Oblique Shock Problems", :keywords => %w(Engineering Calculator Oblique Shock)
  end
  
  def prandtlmeyer
    @title = "Pandtl-Meyer"
    meta :title => @title, :description => "Solves Prandtl-Meyer Problems", :keywords => %w(Engineering Calculator Prandtl Meyer)
  end
  
  def rayleigh
    @title = "Rayleigh Flow"
    meta :title => @title, :description => "Solves Rayleigh Flow Problems", :keywords => %w(Engineering Calculator Rayleigh Flow)
  end
  
end
