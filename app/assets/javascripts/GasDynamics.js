function fanno()
{
	m = parseFloat(document.fanno_form.m_in.value);
	gamma = parseFloat(document.fanno_form.gamma_in.value);
	
	p_ratio = 1/m * 1/Math.sqrt((2/(gamma+1)*(1+(gamma-1)/2*Math.pow(m, 2))));
	rho_ratio = 1/m * Math.sqrt((2/(gamma+1)) * (1+(gamma-1)/2*Math.pow(m, 2)));
	t_ratio = 1/(2/(gamma+1)*(1+(gamma-1)/2*Math.pow(m, 2)));
	u_ratio = m * 1/Math.sqrt(2/(gamma+1) * (1 + (gamma-1)/2*Math.pow(m, 2)));
	po_ratio = 1/m * Math.pow(2/(gamma+1) * (1+(gamma-1)/2*Math.pow(m, 2)),(gamma+1)/(gamma-1)/2);
	fanno_param = (1-Math.pow(m, 2))/(gamma*Math.pow(m, 2)) + (gamma+1)/(gamma*2)*Math.log(Math.pow(m, 2)/(2/(gamma+1)*(1+(gamma-1)/2*Math.pow(m, 2))));
	
	document.getElementById("p_ratio").innerHTML = p_ratio.toFixed(parseInt(4));
	document.getElementById("rho_ratio").innerHTML = rho_ratio.toFixed(parseInt(4));
	document.getElementById("t_ratio").innerHTML = t_ratio.toFixed(parseInt(4));
	document.getElementById("u_ratio").innerHTML = u_ratio.toFixed(parseInt(4));
	document.getElementById("po_ratio").innerHTML = po_ratio.toFixed(parseInt(4));
	document.getElementById("fanno_param").innerHTML = fanno_param.toFixed(parseInt(4));
}

function isentropic_flow()
{
	m = parseFloat(document.isentropic_form.m_in.value);
	gamma = parseFloat(document.isentropic_form.gamma_in.value);
	
	ratio_t = Math.pow(1 + (gamma - 1)/2 * Math.pow(m, 2), -1);
	ratio_p = Math.pow(1 + (gamma - 1)/2 * Math.pow(m, 2), -gamma/(gamma-1));
	ratio_rho = Math.pow(1 + (gamma - 1)/2 * Math.pow(m, 2), -1/(gamma-1));
	ratio_a = Math.pow((gamma + 1)/2, -((gamma + 1)/(gamma - 1)/2))/m * Math.pow(1 + (gamma - 1)/2 * Math.pow(m, 2), ((gamma + 1)/(gamma - 1))/2);
	
	document.getElementById("ratio_t").innerHTML = ratio_t.toFixed(parseInt(4));
	document.getElementById("ratio_p").innerHTML = ratio_p.toFixed(parseInt(4));
	document.getElementById("ratio_rho").innerHTML = ratio_rho.toFixed(parseInt(4));
	document.getElementById("ratio_a").innerHTML = ratio_a.toFixed(parseInt(4));
}

function normal_shock()
{
	mx = parseFloat(document.normal_shock_form.mx_in.value);
	gamma = parseFloat(document.normal_shock_form.gamma_in.value);
	
	my = Math.sqrt((Math.pow(mx,2)*(gamma-1)+2)/(2*gamma*Math.pow(mx,2)-(gamma-1)));
	py_px = 2*gamma* Math.pow(mx,2) /(gamma+1)-(gamma-1)/(gamma+1);
	rhoy_rhox = (gamma+1)*Math.pow(mx,2)/((gamma-1)*Math.pow(mx,2)+2);
	ty_tx = (1 + (gamma - 1)/2 * Math.pow(mx, 2))*(2*gamma/(gamma - 1) * Math.pow(mx, 2) - 1)/(Math.pow(mx, 2)*(2*gamma/(gamma - 1) + (gamma - 1)/2));
	poy_pox = Math.pow((gamma + 1)/2 * Math.pow(mx, 2)/(1 + (gamma - 1)/2 * Math.pow(mx, 2)), gamma/(gamma - 1)) * Math.pow(1/(2 * gamma/(gamma+1) * Math.pow(mx,2) - (gamma-1)/(gamma+1)), 1/(gamma - 1));
	document.getElementById("my").innerHTML = my.toFixed(parseInt(3));
	document.getElementById("py_px").innerHTML = py_px.toFixed(parseInt(4));
	document.getElementById("rhoy_rhox").innerHTML = rhoy_rhox.toFixed(parseInt(4));
	document.getElementById("ty_tx").innerHTML = ty_tx.toFixed(parseInt(4));
	document.getElementById("poy_pox").innerHTML = poy_pox.toFixed(parseInt(4))
}

function oblique()
{
	mx = parseFloat(document.oblique_form.mx_in.value);
	delta = parseFloat(document.oblique_form.delta_in.value);
	gamma = parseFloat(document.oblique_form.gamma_in.value);
	
	pi = Math.PI;
	
	beta = delta*pi/180; // Initial guess that beta and delta coincide.
	e = 1;
	RHS = Math.tan(delta*pi/180);

	while (e >= .00001) {
		LHS = 2*(1/Math.tan(beta))*(Math.pow(mx,2)*Math.pow(Math.sin(beta),2)-1)/(Math.pow(mx,2)*(gamma+Math.cos(2*beta))+2);
		e = RHS - LHS;
		beta = beta + .00001;
	}

	ratio_rho = (gamma+1) * Math.pow(mx, 2)*Math.pow(Math.sin(beta), 2) / ((gamma-1)*Math.pow(mx, 2)*Math.pow(Math.sin(beta), 2)+2);

	beta = beta*180/pi;

	ratio_p = 1+2*gamma/(gamma+1)*(Math.pow(mx,2)*Math.pow(Math.sin(beta*pi/180),2)-1);
	ratio_t = ratio_p*Math.pow((gamma+1)*Math.pow(mx,2)*Math.pow(Math.sin(beta*pi/180),2)/((gamma-1)*Math.pow(mx,2)*Math.pow(Math.sin(beta*pi/180),2)+2),-1);
	my	= (1/Math.sin((beta-delta)*pi/180))*Math.pow((1+.5*(gamma-1)*Math.pow(mx,2)*Math.pow(Math.sin(beta*pi/180),2))/(gamma*Math.pow(mx,2)*Math.pow(Math.sin(beta*pi/180),2)-.5*(gamma-1)),.5);
	ratio_px = Math.pow(1 + (gamma - 1)/2 * Math.pow(mx, 2), -gamma/(gamma-1));
	ratio_py = Math.pow(1 + (gamma - 1)/2 * Math.pow(my, 2), -gamma/(gamma-1));
	ratio_po = ratio_px/ratio_py*ratio_p;

	document.getElementById("my").innerHTML = my.toFixed(parseInt(4));
	document.getElementById("ratio_p").innerHTML = ratio_p.toFixed(parseInt(4));
	document.getElementById("ratio_t").innerHTML = ratio_t.toFixed(parseInt(4));
	document.getElementById("beta").innerHTML = beta.toFixed(parseInt(4));
	document.getElementById("ratio_rho").innerHTML = ratio_rho.toFixed(parseInt(4));
	document.getElementById("ratio_po").innerHTML = ratio_po.toFixed(parseInt(4));
}

function prandtl()
{
	pi = Math.PI;
	mx = parseFloat(document.prandtl_form.mx_in.value);
	gamma = parseFloat(document.prandtl_form.gamma_in.value);
	turningAngle = pi/180*parseFloat(document.prandtl_form.turning_in.value);
	var option = 0
	for (var i=0; i < document.prandtl_form.optionsPrandtl.length; i++)
	{
		if (document.prandtl_form.optionsPrandtl[i].checked == true)
	  {
	  	option = document.prandtl_form.optionsPrandtl[i].value;
	  }
	}
	
	nux = Math.sqrt((gamma+1)/(gamma-1)) * Math.atan(Math.sqrt((gamma-1)/(gamma+1)*(Math.pow(mx, 2)-1))) - Math.atan(Math.sqrt(Math.pow(mx, 2)-1));
	nuy = 0;
	
	if ( option == 1) {
		nuy = nux - turningAngle;
	} else {
		nuy = nux + turningAngle;
	}
	
	my = 1;
	e = 1;
	while (e >= .00001) {
		nuy_test = Math.sqrt((gamma+1)/(gamma-1)) * Math.atan(Math.sqrt((gamma-1)/(gamma+1)*(Math.pow(my, 2)-1))) - Math.atan(Math.sqrt(Math.pow(my, 2)-1));
		e = nuy - nuy_test;
		my = my+.00001;
	}
	my = my-.00001;
	
	ty_tx = (1+(gamma-1)/2*Math.pow(mx, 2))/(1+(gamma-1)/2*Math.pow(my, 2));
	py_px = Math.pow((1+(gamma-1)/2*Math.pow(mx, 2))/(1+(gamma-1)/2*Math.pow(my, 2)), gamma/(gamma-1));
	rhoy_rhox = Math.pow((1+(gamma-1)/2*Math.pow(mx, 2))/(1+(gamma-1)/2*Math.pow(my, 2)), 1/(gamma-1));
	mux = Math.asin(1/mx);
	muy = Math.asin(1/my);
	
	document.getElementById("t_ratio").innerHTML = ty_tx.toFixed(parseInt(4));
	document.getElementById("p_ratio").innerHTML = py_px.toFixed(parseInt(4));
	document.getElementById("rho_ratio").innerHTML = rhoy_rhox.toFixed(parseInt(4));
	document.getElementById("my").innerHTML = my.toFixed(parseInt(4));
	document.getElementById("nux").innerHTML = ((nux*180)/pi).toFixed(parseInt(4));
	document.getElementById("nuy").innerHTML = ((nuy*180)/pi).toFixed(parseInt(4));
	document.getElementById("mux").innerHTML = ((mux*180)/pi).toFixed(parseInt(4));
	document.getElementById("muy").innerHTML = ((muy*180)/pi).toFixed(parseInt(4));
	
}

function rayleigh()
{
	m = parseFloat(document.rayleigh_form.m_in.value);
	gamma = parseFloat(document.rayleigh_form.gamma_in.value);
	
	p_ratio = (gamma+1)/(1+gamma*Math.pow(m, 2));
	rho_ratio = (1+gamma*Math.pow(m,2))/((1+gamma)*Math.pow(m,2));
	t_ratio = Math.pow(gamma+1, 2)*Math.pow(m, 2)/Math.pow(1+gamma*Math.pow(m, 2), 2);
	u_ratio = (gamma+1)*Math.pow(m, 2)/(1+gamma*Math.pow(m, 2));
	po_ratio = (gamma+1)/(1+gamma*Math.pow(m, 2)) * Math.pow(2/(gamma+1)*(1+(gamma-1)/2*Math.pow(m, 2)), gamma/(gamma-1));
	to_ratio= 2*(gamma+1)*Math.pow(m, 2)/Math.pow(1+gamma*Math.pow(m, 2),2) * (1+(gamma-1)/2*Math.pow(m, 2));
	
	document.getElementById("p_ratio").innerHTML = p_ratio.toFixed(parseInt(4));
	document.getElementById("rho_ratio").innerHTML = rho_ratio.toFixed(parseInt(4));
	document.getElementById("t_ratio").innerHTML = t_ratio.toFixed(parseInt(4));
	document.getElementById("u_ratio").innerHTML = u_ratio.toFixed(parseInt(4));
	document.getElementById("po_ratio").innerHTML = po_ratio.toFixed(parseInt(4));
	document.getElementById("to_ratio").innerHTML = to_ratio.toFixed(parseInt(4));
	
}